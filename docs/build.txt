On the topic of building:

Build in Release mode:
    - In debug mode the HOG&SVM is painfully slow
    - if you use catkin_make, cmake chooses to ignore options and defines in the cmake files
      thus use: catkin_make -DCMAKE_BUILD_TYPE=Release

Use Vector extensions:
    - AVX is best, then comes SSE4
    - Dlib automatically detects the supported extension, but this fails with catkin_make
    - Although the CMakeLists.txt file sets the options (USE_AVX_INSTRUCTIONS, USE_SEE4_INSTRUCTIONS)
      catkin_make also overrides those somehow, if you want to build with catkin and not use AVX use:
      catkin_make -DCMAKE_BUILD_TYPE=Release -DUSE_AVX_INSTRUCTIONS=OFF -DUSE_SSE4_INSTRUCTIONS=ON

Maybe the problems above can be solved by a heroic catkin expert, i always use the cmake files on it's own.


