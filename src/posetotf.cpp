#include <ros/ros.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

void poseCallback(geometry_msgs::PoseStamped msg) {
    static tf::TransformBroadcaster br;

    auto p = msg.pose.position;
    auto q = msg.pose.orientation;

    tf::Transform transform;

    transform.setOrigin(tf::Vector3(p.x, p.y, p.z));
    transform.setRotation(tf::Quaternion(q.x, q.y, q.z, q.w));

    br.sendTransform(tf::StampedTransform(transform, msg.header.stamp, "base_link", "operator_tf"));
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "posetotf");
    ros::NodeHandle node;

    ros::Subscriber sub = node.subscribe("/operator_pose", 10, &poseCallback);

    // in case bag files don't have the camera transform
    /*
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    geometry_msgs::TransformStamped static_transformStamped;
    static_transformStamped.header.stamp = ros::Time::now();
    static_transformStamped.header.frame_id = "base_link";
    static_transformStamped.child_frame_id = "camera2";
    static_transformStamped.transform.translation.x = 0.25;
    static_transformStamped.transform.translation.y = 0;
    static_transformStamped.transform.translation.z = 0.5;
    tf::Quaternion q;
    q.setRPY(0, -14.0f / 180 * M_PI, 0);
    static_transformStamped.transform.rotation.x = q.x();
    static_transformStamped.transform.rotation.y = q.y();
    static_transformStamped.transform.rotation.z = q.z();
    static_transformStamped.transform.rotation.w = q.w();
    static_broadcaster.sendTransform(static_transformStamped);
    */

    ros::spin();
    return 0;
};

