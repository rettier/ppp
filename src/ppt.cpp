#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <std_msgs/String.h>

#include "ppt/PPT.h"
#include "ppt/ROSInterface.h"

int main(int argc, char **argv) {
    ros::init(argc, argv, "ppt");

    // variables stuff
    ppt::PPT ppt;
    ppt::ROSInterface pptROS(&ppt);

    // init stuff
    pptROS.init();

    // run stuff
    ppt.start();
    pptROS.spin();

    // wait for shutdown stuff
    ppt.stop();
    ppt.join();

    // return stuff
    return 0;

    // much stuff
}