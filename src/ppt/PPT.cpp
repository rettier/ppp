//
// Created by reitph on 22.01.17.
//

#include "PPT.h"

PPT_NS_START


    PPT::PPT() : targetId(-1), WorkerThread("PPT"), confidence(0), trackerThread(this) {
    }

    PPT::~PPT() {

    }

    void PPT::init() {
        svmThread.start();
        trackerThread.start();
    }

    void PPT::collectStates() {
        svmThread.state.lock();
        if (svmThread.state.var.timestamp > svmState.timestamp) {
            svmState = svmThread.state.var;

            svmHistory.push_front(svmState);
            if (svmHistory.size() > HISTORY_SIZE)
                svmHistory.pop_back();
        }
        svmThread.state.unlock();

        trackerState = trackerThread.state.get();
    }

    void PPT::killOldTracks(TrackerCommandVector &commands) {
        auto now = ros::Time::now();
        for (auto &state : trackerState) {
            if (state.state != LOST)
                continue;

            if ((now - state.lostAt).toSec() > LOST_TIMEOUT)
                commands.push_back(TrackerCommandMessage::kill(state.id));
        }
    }

    void PPT::spawnNewTracker(TrackerCommandVector &commands,
                              cv::Rect2f &svmRect, ros::Time timestamp) {
        commands.push_back(TrackerCommandMessage::init(svmRect, timestamp));
    }

    void PPT::maintainTracker(TrackerCommandVector &commands, StampedTrackerState trackerState, cv::Rect2f svmRect,
                              ros::Time timestamp, StampedROSState *rosState) {
        float trackerArea = trackerState.rect.area();
        float offcenter = std::abs(svmRect.x - trackerState.rect.x) / std::min(svmRect.width, trackerState.rect.width);
        float svmArea = svmRect.area();

        bool reinit = false;
        if (svmArea < trackerArea * AREA_REINIT_PERCENT_LOWER) {
            reinit = true;
            log("requesting reinit due to: svmArea < trackerArea * factor");
        } else if (trackerArea / svmArea < AREA_REINIT_PERCENT_GREATER) {
            reinit = true;
            log("requesting reinit due to: trackerArea / svmArea < factor");
        } else if (offcenter > OFFCENTER_REINIT) {
            reinit = true;
            log("requesting reinit due to: offcenter: %f", offcenter);
        }

        if (reinit)
            commands.push_back(TrackerCommandMessage::reinit(svmRect, timestamp, trackerState.id));

        if (trackerState.id == targetId) {
            lastSVMMatch = svmState.timestamp;
            lastSVMSize = trackerState.rect;
        }
    }

    void PPT::processSvmDetections(TrackerCommandVector &commands) {
        if (svmState.timestamp == lastProcessedSvmState)
            return;

        StampedROSState *stateForSvm = nullptr;
        for (auto &state : stateHistory) {
            if (state.timestamp == svmState.timestamp) {
                stateForSvm = &state;
                break;
            }
        }

        for (auto &svm : svmState.detections) {
            auto rect = dlib2cv(svm);
            TrackerThread::padRect(rect);

            StampedTrackerState *state = nullptr;
            for (auto &tracker : trackerState) {
                if (tracker.state != TRACKING)
                    continue;

                if (rectOverlap(tracker.rect, rect) > AREA_MATCH_PERCENT) {
                    state = &tracker;
                    break;
                }
            }

            if (state != nullptr) {
                maintainTracker(commands, *state, rect, svmState.timestamp, stateForSvm);
            } else {
                spawnNewTracker(commands, rect, svmState.timestamp);
            }
        }
        lastProcessedSvmState = svmState.timestamp;
    }

    bool PPT::canSendNewCommands() {
        trackerThread.command.lock();
        bool res = trackerThread.command.var.size() == 0;
        trackerThread.command.unlock();
        return res;
    }

    void PPT::work() {
        svmThread.queueData(currentState);
        trackerThread.queueData(currentState);

        stateHistory.push_front(currentState);
        if (stateHistory.size() > HISTORY_SIZE)
            stateHistory.pop_back();

        collectStates();
        if (canSendNewCommands()) {
            TrackerCommandVector commands;
            processSvmDetections(commands);
            killOldTracks(commands);
            trackerThread.command.set(commands);
        }

        updateHypothesis();
        publishResults();
        showDebugImage();
    }

    void PPT::publishResults() {
        this->publish->publishPoses(this->trackerState);
        this->publish->publishTargetTrack(stateHistory.front());
    }

    void PPT::shutdown() {
        svmThread.stop();
        trackerThread.stop();
        svmThread.join();
        trackerThread.join();
    }

    // unused
    bool PPT::calculateBEM(StampedTrackerState &state) {
        KcfParameters trackerParams;
        trackerParams.enableTrackingLossDetection = true;
        trackerParams.psrThreshold = 13.5;
        auto *tracker = new KcfTracker(trackerParams);
        bool init = false;
        cv::Rect2f rect = state.rect;
        int steps = 0;
        for (auto it = stateHistory.begin(); it != stateHistory.end(); ++it) {
            bool tracking;
            if (!init) {
                tracking = tracker->reinit(it->cameraFrame, rect);
            } else {
                tracking = tracker->update(it->cameraFrame, rect);
            }
            cv::Rect2f overlap = constrainRect(rect) & constrainRect(it->targetRect);
            float percent = overlap.area() / std::min(rect.area(), it->targetRect.area());
            if (steps > 15 && percent > 70 && tracking)
                return true;
            init = true;
        }
        return false;
    }

    void PPT::updateHypothesis() {
        StampedTrackerState *target = nullptr;
        if (targetId >= 0) {
            bool found = false;
            for (auto &tracker : trackerState) {
                if (tracker.id == targetId) {
                    found = true;
                    target = &tracker;
                    break;
                }
            }
            if (!found) {
                targetId = -1;
                confidence = 0;
            }
        }

        if (currentState.target_command == ppt::target::Request::CLEAR ||
            currentState.target_command == ppt::target::Request::REINIT) {
            targetId = -2;
        }

        if (targetId == -2) {
            // search for a new target, this code takes the next tracker that matches for some
            // randomly chosen amount of time, so one wrong operator pose won't select a wrong target
            for (auto &state : trackerState) {
                float w2 = state.rect.width * 0.5f;
                float centerX = state.rect.x + w2;
                float diff = (float) fabs(currentState.operatorTop.x - centerX);
                if (diff < w2) {
                    targetScores[state.id] += (1.0f - diff / w2);
                    if (targetScores[state.id] > 5.f) {
                        targetId = state.id;
                        targetScores.clear();
                        confidence = 1.0;
                        lastSVMMatch = svmState.timestamp;
                        lastSVMSize = state.rect;
                        break;
                    }
                }
            }
        }

        if (target != nullptr) {
            // cost function calculation
            float svmTrustTime = 0.5f;
            float growPenalty = 0.5f;
            float grothFactor = 0.3f;
            confidence = 1.0f;
            float diff = (float) (currentState.timestamp - lastSVMMatch).toSec();
            if (diff > svmTrustTime) {
                float wh = -std::min(0.f, 1.0f - target->rect.height / lastSVMSize.height);
                float ww = -std::min(0.f, 1.0f - target->rect.width / lastSVMSize.width);
                float outside = std::max(0.f, 1.0f - constrainRect(target->rect).area() / target->rect.area());
                float groth = (float) fmax(wh, ww);
                confidence -= (fmin(grothFactor, groth) / grothFactor * growPenalty);
                confidence -= std::min(outside / 0.6f, 0.6f) * groth * 0.3f;
                //printf("penalty: %01.2f, groth: %01.2f, outside: %01.2f\n",
                //       std::min(outside / 0.6f, 0.6f) * groth * 0.3f, groth, outside);
            }

            if (confidence <= 0.5f) {
                targetId = -1;
            }

            stateHistory.front().targetId = targetId;
            stateHistory.front().targetRect = target->rect;
            stateHistory.front().confidence = confidence;
        } else {
            stateHistory.front().targetId = -1;
            stateHistory.front().confidence = 0;
        }
    }

    void PPT::showDebugImage() {
        if (!debug || currentState.cameraFrame.empty())
            return;

        cv::Mat debugImage = currentState.cameraFrame.clone();

        const auto operatorColor = cv::Scalar(255, 0, 0);
        const auto trackerColor = cv::Scalar(0, 255, 255);
        const auto targetColor = cv::Scalar(0, 255, 0);
        const auto svmColor = cv::Scalar(0, 0, 255);

        if (currentState.hasOperatorPose)
            cv::rectangle(debugImage, currentState.getOperatorRect(), operatorColor, 1);

        if ((currentState.timestamp - svmState.timestamp).toSec() < 0.1) {
            for (auto &rect : svmState.detections) {
                cv::rectangle(debugImage, dlib2cv(rect), svmColor, 1);
            }
        }

        for (auto &tracker : trackerState) {
            if (tracker.state == TRACKING) {
                auto color = targetId == -1 ? svmColor : (tracker.id == targetId ? targetColor : trackerColor);
                if (confidence < 0.60)
                    color = svmColor;
                cv::rectangle(debugImage, tracker.rect, color, tracker.state == TRACKING ? 2 : 1);
            }
        }
        int fontFace = CV_FONT_NORMAL;
        double fontScale = 1;
        int thickness = 3;

        const char *text = targetId == -1 ? "lost" : "locked";

        int baseline = 0;
        cv::Size textSize = cv::getTextSize(text, fontFace,
                                            fontScale, thickness, &baseline);
        baseline += thickness;

        cv::Point textOrg((debugImage.cols - textSize.width) / 2,
                          debugImage.rows + (-textSize.height) / 2);

        auto color = targetId == -1 || confidence < 0.60f ? svmColor : targetColor;
        cv::putText(debugImage, text, textOrg, fontFace, fontScale, color, thickness, 8);

        textOrg = cv::Point((debugImage.cols - textSize.width) / 2,
                            (textSize.height));

        std::stringstream ss;
        ss.precision(2);
        ss << confidence;
        cv::putText(debugImage, ss.str().c_str(), textOrg, fontFace, fontScale, color, thickness, 8);

        debug->showImage(debugImage);
    }


PPT_NS_END
