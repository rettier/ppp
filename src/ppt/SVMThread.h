//
// Created by reitph on 22.01.17.
//

#ifndef PPT_SVMTHREAD_H
#define PPT_SVMTHREAD_H

#include <dlib/svm_threaded.h>
#include <dlib/string.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_processing.h>
#include <dlib/data_io.h>
#include <dlib/cmd_line_parser.h>
#include <dlib/opencv.h>

#include "common.h"
#include "worker.h"

PPT_NS_START

    class PPT;


    struct StampedSVMDetection {
        using RectVector = std::vector<dlib::rectangle>;

        ros::Time timestamp;
        RectVector detections;

        StampedSVMDetection() {

        }

        StampedSVMDetection(const RectVector &detections, ros::Time timestamp)
                : detections(detections),
                  timestamp(timestamp) {
        }
    };

    class SVMThread : public PPTWorker {

        static constexpr const char *DETECTOR_PATH = "data/detector.svm";
        typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<6> > image_scanner_type;

    public:
        SVMThread();

        LockedVariable<StampedSVMDetection> state;

        ~SVMThread() {}

    private:
        dlib::object_detector<image_scanner_type> detector;

        virtual void init();

        virtual void work();

        virtual void shutdown();
    };

PPT_NS_END

#endif // PPT_SVMTHREAD_H
