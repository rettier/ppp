#ifndef __WORKER_H__
#define __WORKER_H__

#include "common.h"
#include <geometry_msgs/PoseArray.h>
#include <ppt/target.h>

namespace ppt {

    class StampedTrackerState;

    using TrackerStateVector = std::vector<StampedTrackerState>;
    struct StampedROSState;

    class PPTPublishInterface {
    public:
        virtual void publishPoses(const TrackerStateVector &state) const = 0;
        virtual void publishTargetTrack(const StampedROSState &state) const = 0;
    };

    class PPTDebugInterface {
    public:
        virtual void showImage(const cv::Mat &mat) const = 0;
    };

    template<typename T>
    class LockedVariable {
    public:
        inline void set(const T &newV) {
            guard(mutex);
            var = newV;
        }

        inline T get() {
            guard(mutex);
            return var;
        }

        inline void lock() {
            mutex.lock();
        }

        inline void unlock() {
            mutex.unlock();
        }

        std::mutex mutex;
        T var;
    };

    template<typename T, int M>
    class WorkerThread {
        using DataQueue = std::deque<T>;
        const int QueueCapacity = M;
        using Time = std::chrono::high_resolution_clock;
        using TimePoint = std::chrono::high_resolution_clock::time_point;

    protected:
        T currentState;

    private:
        DataQueue queue;
        std::condition_variable queueCondition;
        std::mutex queueMutex;
        std::thread thread;
        const char *name;

        int frameCount = 0;
        TimePoint lastTime = Time::now();
        bool idle = true;
        bool run = true;

        virtual bool waitForNextState() {
            guard lock(queueMutex);
            while (queue.empty()) {
                queueCondition.wait(lock);
                if (!run)
                    return false;
            }

            currentState = queue.back();
            queue.pop_back();
            return true;
        }

        virtual void init() = 0;

        virtual void work() = 0;

        virtual void shutdown() = 0;

        void fps() {
            frameCount += 1;
            int diff = std::chrono::duration<double, std::milli>(Time::now() - lastTime).count();
            if (diff > 1000) {
                lastTime = Time::now();
                float fps = 1000 * frameCount / (float) diff;
                if (fps < 20)
                    log("FPS: %.2f", fps);
                frameCount = 0;
            }
        }

    public:
        WorkerThread(const char *name) : name(name) {}

        virtual ~WorkerThread() {};

        inline bool isIdle() { return idle; }

        inline bool isRunning() { return run; }

        inline void stop() {
            guard(queueMutex);
            run = false;
            queueCondition.notify_all();
        }

        inline void join() { thread.join(); }

        virtual void queueData(const T &t) {
            guard lock(queueMutex);
            if (queue.size() >= QueueCapacity)
                return;
            queue.push_front(t);
            queueCondition.notify_all();
        }

        virtual void spin() {
            while (run) {
                if (!waitForNextState())
                    break;

                work();
                fps();
            }
            shutdown();
        }

        void start() {
            thread = std::thread([&] {
                this->init();
                this->spin();
            });
        }

        void log(const char *format, ...) __attribute__((format (printf, 2, 3))) {
            char buffer[256];
            va_list args;
            va_start (args, format);
            vsnprintf(buffer, 255, format, args);

            printf("[%10s] ", name);
            puts(buffer);

            va_end (args);
        }

    };

    struct StampedROSState {
        cv::Mat cameraFrame;
        cv::Mat cameraFrameGray;

        uint8_t target_command;
        bool hasOperatorPose = false;
        uv operatorBot;
        uv operatorTop;
        cv::Rect2f targetRect;
        int targetId;
        float confidence;

        ros::Time timestamp;

        StampedROSState() {
        }

        void reset() {
            target_command = ppt::target::Request::UNCHANGED;
        }

        cv::Rect2f getOperatorRect() {
            constexpr float aspectRatio = 0.4f;
            float height = operatorTop.y - operatorBot.y;
            float width = height * aspectRatio;
            return cv::Rect2f(
                    operatorBot.x - width * 0.5f,
                    operatorBot.y,
                    width,
                    height
            );
        }
    };

    using PPTWorker = WorkerThread<StampedROSState, 1>;
}

#endif