//
// Created by ros on 27.01.17.
//

//#include <opencv-3.1.0-dev/opencv2/opencv.hpp>
#include "Hypothesis.h"

namespace ppt {

    Histogram::Histogram() : init(false) {
        int dims[3] = {BUCKET_COUNT, BUCKET_COUNT, BUCKET_COUNT};
        hist = cv::Mat(3, dims, CV_32F);
        hist.setTo(cv::Scalar(0));
    }

    void Histogram::update(cv::Mat &img, cv::Mat &mask) {
        if (!init) {
            reset(img, mask);
            return;
        }
        cv::Mat newHist;
        getFullHist(img, newHist, mask);
        newHist = newHist * (1.0f / cv::sum(newHist)[0]);
        hist = hist * (1.0f - LEARNING_RATE) + newHist * LEARNING_RATE;
    }

    void Histogram::reset(cv::Mat &img, cv::Mat &mask) {
        init = true;
        getFullHist(img, hist, mask);
        hist = hist * (1.0f / cv::sum(hist)[0]);
    }

    void Histogram::getFullHist(cv::Mat &image, cv::Mat &dst, cv::InputArray mask) {
        int channels[3] = {0, 1, 2};
        int size[3] = {BUCKET_COUNT, BUCKET_COUNT, BUCKET_COUNT};
        const float range[2] = {0, 256};
        const float *ranges[3] = {range, range, range};
        cv::calcHist(&image, 1, channels, mask, dst, 3, size, ranges);
    }

    const char *name;

    float Hypothesis::match(cv::Mat &img, cv::Rect2f rect, Histogram &hist) {
        rect = constrainRect(rect);
        cv::Mat roi = img(rect).clone();
        roi.convertTo(roi, CV_32FC3);
        float bin_width = 255.0f / (Histogram::BUCKET_COUNT - 1);
        roi /= bin_width;
        roi.convertTo(roi, CV_8UC3);
        cv::Mat probMap(roi.rows, roi.cols, CV_32F);

        for (int x = 0; x < roi.cols; ++x) {
            for (int y = 0; y < roi.rows; ++y) {
                cv::Vec3b &pixel = roi.at<cv::Vec3b>(y, x);
                float pBg = bgHist.hist.at<float>(pixel[0], pixel[1], pixel[2]);
                float pFg = hist.hist.at<float>(pixel[0], pixel[1], pixel[2]);
                if (pFg + pBg == 0)
                    probMap.at<float>(y, x) = 0.5;
                else {
                    probMap.at<float>(y, x) = pFg / (pFg + pBg);
                }
            }
        }

        cv::imshow(name, probMap);
        cv::Mat integral;
        cv::integral(probMap, integral);
        double prob = integral.at<double>(0, 0) + integral.at<double>(integral.rows - 1, integral.cols - 1) -
                      integral.at<double>(integral.cols - 1, 0) - integral.at<double>(0, integral.cols - 1);
        prob = log(prob);
        return (float) prob;
    }

/*
    Hypothesis::Hypothesis() :
            mask(480, 640, CV_8UC1) {
        int dims[3] = {BUCKET_COUNT, BUCKET_COUNT, BUCKET_COUNT};
        histFg = cv::Mat(3, dims, CV_32F);
        histBg = cv::Mat(3, dims, CV_32F);
        histFg.setTo(cv::Scalar(0));
        histBg.setTo(cv::Scalar(0));
    }*/

    Hypothesis::Hypothesis() :
            mask(480, 640, CV_8UC1) {

    }

    void Hypothesis::getBgMask(cv::Mat &image, const TrackerStateVector &rects) {
        mask.setTo(cv::Scalar(255));
        auto scale = cv::Size2f(1.0, 1.0);
        for (auto &state : rects) {
            auto rect = state.rect;
            cv::Vec2f center(rect.x + rect.width * 0.5f, rect.y + rect.height * 0.5f);
            cv::Vec2f size = cv::Vec2f(rect.width * 0.5f * scale.width, rect.height * 0.5f * scale.height);
            cv::Vec2i tl = center - size;
            cv::Vec2i br = center + size;
            br[0] = std::max(0, std::min(br[0], image.cols - 1));
            br[1] = std::max(0, std::min(br[1], image.rows - 1));
            tl[0] = std::max(0, std::min(tl[0], image.cols - 1));
            tl[1] = std::max(0, std::min(tl[1], image.rows - 1));
            mask(cv::Rect2i(tl, br)) = 0;
        }
    }

    void Hypothesis::update(cv::Mat &image, const TrackerStateVector &rects) {
        if (rects.size() == 0)
            return;

        getBgMask(image, rects);
        bgHist.update(image, mask);

        auto noMask = cv::Mat();
        for (auto &state : rects) {
            int id = state.id;
            Histogram &hist = hists[id];

            auto rect = constrainRect(state.rect);
            rect = scaleRect(rect, cv::Size2f(0.6, 0.9));

            auto cutout = image(rect);
            hist.update(cutout, noMask);

            std::stringstream ss;
            ss << "probmap" << state.id;
            name = ss.str().c_str();
            match(image, rect, hist);
        }
        cv::waitKey(1);
    }

    void Hypothesis::matchAll(cv::Mat &img, cv::Rect2f rect) {
        rect = constrainRect(rect);
        rect = scaleRect(rect, cv::Size2f(0.6, 0.9));

        for (auto &entry : hists) {
            printf("[%3d]: %2.1f\n", entry.first, match(img, rect, entry.second));
        }
    }

}

