//
// Created by ros on 27.01.17.
//

#ifndef PPT_HYPOTHESIS_H
#define PPT_HYPOTHESIS_H

#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include "TrackerThread.h"

namespace ppt {
    class Histogram {
    public:

        cv::Mat hist;
        bool init;

        static constexpr int BUCKET_COUNT = 8;
        static constexpr float LEARNING_RATE = 0.1f;

        Histogram();

        void update(cv::Mat &img, cv::Mat &mask);

        void getFullHist(cv::Mat &image, cv::Mat &dst, cv::InputArray mask);

        void reset(cv::Mat &img, cv::Mat &mask);
    };

    class Hypothesis {


    public:
        Hypothesis();

        void update(cv::Mat &img, const TrackerStateVector &rects);

        void getBgMask(cv::Mat &image, const TrackerStateVector &rects);

        void getMask(cv::Mat &image, cv::Rect2f rect, cv::Size2f scale);

        float match(cv::Mat &img, cv::Rect2f rect, Histogram &hist);

        void matchAll(cv::Mat& img, cv::Rect2f rect);

    private:
        cv::Mat mask;
        Histogram bgHist;
        std::map<int, Histogram> hists;
    };
}

#endif //PPT_HYPOTHESIS_H
