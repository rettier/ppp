//
// Created by reitph on 22.01.17.
//

#ifndef PPT_ROS_H
#define PPT_ROS_H

#include <ppt/target.h>
#include <ppt/trackertarget.h>
#include <ppt/status.h>
#include "common.h"
#include "worker.h"

PPT_NS_START


    class PPT;

    class StampedTrackerState;

    using TrackerStateVector = std::vector<StampedTrackerState>;

    class ROSInterface : PPTDebugInterface, PPTPublishInterface {

        static constexpr const char *IMAGE_TOPIC = "/camera/rgb/image_raw";
        static constexpr const char *DEBUG_TOPIC = "/ppt/image_debug";
        static constexpr const char *STATUS_TOPIC = "/ppt/status";
        static constexpr const char *TARGET_STATUS_TOPIC = "/ppt/target/status";
        static constexpr const char *TARGET_TRACK_TOPIC = "/ppt/target/track";
        static constexpr const char *TARGET_POSE_TOPIC = "/ppt/target/pose";
        static constexpr const char *POSE_TOPIC = "/ppt/humans";
        static constexpr const char *TF_OPERATOR = "operator_tf";
        static constexpr const char *TF_CAMERA = "camera_link";

        static constexpr float OPERATOR_HEIGHT = 1.45f; // has no effect, only for debug visuals
        static constexpr float OPERATOR_POSE_TIMEOUT = 0.1f;

        using uvPair = std::tuple<cv::Point2f, cv::Point2f>;

    public:
        ROSInterface(PPT *ppt);

        virtual ~ROSInterface() {}

        void init();

        void spin();

        virtual void showImage(const cv::Mat &mat) const;

        virtual void publishPoses(const TrackerStateVector &state) const;

        virtual void publishTargetTrack(const StampedROSState &state) const;

    protected:
        tf::TransformListener tfListener;
        ros::NodeHandle nodeHandle;
        it::ImageTransport imageTransport;
        it::CameraSubscriber cameraSubscriber;
        ros::ServiceServer statusService;
        ros::ServiceServer targetService;
        ig::PinholeCameraModel cameraModel;
        tf::StampedTransform lastOperatorTransform;
        it::Publisher debugImagePublisher;
        ros::Publisher poseArrayPublisher;
        ros::Publisher trackPublisher;
        ros::Publisher targetPosePublisher;

        PPT *ppt;
        StampedROSState state;
        bool enabled;

        void onCameraFrame(const sensor_msgs::ImageConstPtr &image,
                           const sensor_msgs::CameraInfoConstPtr &info);

        ROSInterface::uvPair transformOperatorToImage(const tf::StampedTransform &transform);

        bool setStatus(ppt::status::Request &req, ppt::status::Response &res);

        bool setTarget(ppt::target::Request &req, ppt::target::Response &res);

    };

PPT_NS_END

#endif //PPT_ROS_H
