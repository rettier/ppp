//
// Created by reitph on 23.01.17.
//

#ifndef PPT_TRACKERTHREAD_H
#define PPT_TRACKERTHREAD_H

#include "common.h"

#include "kcf_tracker.hpp"
#include "worker.h"

PPT_NS_START

    using cf_tracking::KcfParameters;
    using cf_tracking::KcfTracker;

    enum TrackerCommand {
        NO_COMMAND = 0,
        INIT_FROM_BACKLOG = 1,
        KILL = 2,
        REINIT_FROM_BACKLOG = 3,
    };

    struct TrackerCommandMessage {
        int id;
        TrackerCommand command;
        cv::Rect2f rect;
        ros::Time timestamp;

        static TrackerCommandMessage kill(int id) {
            return TrackerCommandMessage{id: id, command: KILL};
        }

        static TrackerCommandMessage reinit(cv::Rect2f &rect, ros::Time &time, int id) {
            return TrackerCommandMessage{
                    id: id,
                    command: REINIT_FROM_BACKLOG,
                    rect: rect,
                    timestamp: time
            };
        }

        static TrackerCommandMessage init(cv::Rect2f &rect, ros::Time &time) {
            return TrackerCommandMessage{
                    id: 0,
                    command: INIT_FROM_BACKLOG,
                    rect: rect,
                    timestamp: time
            };
        }
    };

    enum TrackerState {
        BACKLOG = 0,
        TRACKING = 1,
        LOST = 2,
        IDLE = 3
    };

    struct StampedTrackerState {
        int id;
        TrackerState state = BACKLOG;
        cv::Rect2f rect;
        ros::Time timestamp;
        ros::Time lostAt;
    };

    using TrackerCommandVector = std::vector<TrackerCommandMessage>;
    using TrackerStateVector = std::vector<StampedTrackerState>;

    class PPT;

    class TrackerThread : public PPTWorker {
        static constexpr int HISTORY_SIZE = 30;
        static constexpr float DEDUPLICATE_THRESH = 0.6f;
        static constexpr float TRACKER_MARGIN_WIDTH = 0.10; // in percent
        static constexpr float TRACKER_MARGIN_HEIGHT = 0.08;

    public:

        TrackerThread(PPT *);

        ~TrackerThread();

        LockedVariable<TrackerCommandVector> command;
        LockedVariable<TrackerStateVector> state;

        static void padRect(cv::Rect2f &rect);

    private:

        PPT *parent;

        int lastId = 1;

        std::deque<StampedROSState> history;

        std::map<int, KcfTracker *> trackers;
        KcfParameters trackerParams;

        virtual void init();

        virtual void work();

        virtual void shutdown();

        TrackerStateVector::iterator stateById(TrackerStateVector &states, int id);

        void deduplicate(TrackerStateVector &states);

        void processCommands(TrackerStateVector &states, TrackerCommandVector &commands);

        void newTrackerFromBacklog(TrackerCommandMessage &cmd, TrackerStateVector &variable);

        void reinitTracker(TrackerCommandMessage &cmd, TrackerStateVector::iterator &currentStateIt,
                           TrackerStateVector &states);

        void track(TrackerStateVector &state);

    };

PPT_NS_END

#endif //PPT_TRACKERTHREAD_H
