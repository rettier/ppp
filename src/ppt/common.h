#ifndef __COMMON_H__
#define __COMMON_H__

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <image_geometry/pinhole_camera_model.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <std_msgs/String.h>
#include <tuple>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <iostream>
#include <queue>
#include <chrono>
#include <dlib/image_processing.h>

#define PPT_LOG(args) do { printf(args "\n"); ROS_DEBUG(args); } while(0)
#define PPT_NS_START namespace ppt {
#define PPT_NS_END }

PPT_NS_START

    namespace it = image_transport;
    namespace ig = image_geometry;

    using uv = cv::Point2f;
    using guard = std::unique_lock<std::mutex>;

    using std::string;
    using std::make_tuple;
    using std::tie;

    static cv::Rect2f constrainRect(cv::Rect2f rect, float border = 0) {
        rect.x = std::max(rect.x, border);
        rect.y = std::max(rect.y, border);
        rect.width = std::min(rect.width, 640 - rect.x - border);
        rect.height = std::min(rect.height, 480 - rect.y - border);
        return rect;
    }

    static cv::Rect2f scaleRect(cv::Rect2f rect, cv::Size2f scale) {
        cv::Vec2f center(rect.x + rect.width * 0.5f, rect.y + rect.height * 0.5f);
        cv::Vec2f size = cv::Vec2f(rect.width * 0.5f * scale.width, rect.height * 0.5f * scale.height);
        cv::Vec2f tl = center - size;
        cv::Vec2f br = center + size;
        return cv::Rect2f(tl, br);
    }

    static inline cv::Point3d tf2cv(const tf::Point &pt) {
        return cv::Point3d(pt.y(), pt.z(), pt.x());
    };

    static inline cv::Rect2f dlib2cv(const dlib::rectangle &r) {
        return cv::Rect2f(r.left(), r.top(), r.width(), r.height());
    }

    static float rectOverlap(cv::Rect2f r1, cv::Rect2f r2) {
        cv::Rect2f overlap = r1 & r2;
        return overlap.area() / std::max(r1.area(), r2.area());
    }


PPT_NS_END

#endif