//
// Created by reitph on 22.01.17.
//

#ifndef PPT_PPT_H
#define PPT_PPT_H

#include "common.h"
#include "SVMThread.h"
#include "TrackerThread.h"
#include "Hypothesis.h"
#include "worker.h"

PPT_NS_START


    class PPT : public PPTWorker {

        // store 2 seconds of frames
        static constexpr int HISTORY_SIZE = 30 * 1;

        // after 0.5 sec we kill the convolution tracker
        static constexpr float LOST_TIMEOUT = 0.5f;

        // area the svm needs to match with the tracker in order to be
        // identified as the same object/person
        static constexpr float AREA_MATCH_PERCENT = 0.4f;

        // reinit conditions
        static constexpr float AREA_REINIT_PERCENT_GREATER = 0.38f;
        static constexpr float AREA_REINIT_PERCENT_LOWER = 0.90f;
        static constexpr float OFFCENTER_REINIT = 0.3f;

    private:
        SVMThread svmThread;
        TrackerThread trackerThread;

        void showDebugImage();

        std::deque<StampedROSState> stateHistory;
        std::deque<StampedSVMDetection> svmHistory;
        ros::Time lastProcessedSvmState;

        StampedSVMDetection svmState;
        TrackerStateVector trackerState;

        float confidence;
        ros::Time lastSVMMatch;
        cv::Rect2f lastSVMSize;
        std::map<int, float> targetScores;

    public:
        PPT();

        virtual ~PPT();

        virtual void init();

        virtual void work();

        virtual void shutdown();

        PPTDebugInterface *debug = nullptr;
        PPTPublishInterface *publish = nullptr;

        void updateHypothesis();

        void collectStates();

        void killOldTracks(TrackerCommandVector &commands);

        void processSvmDetections(TrackerCommandVector &commands);

        bool canSendNewCommands();

        void maintainTracker(TrackerCommandVector &commands, StampedTrackerState trackerState, cv::Rect2f svmRect,
                             ros::Time timestamp, StampedROSState *pState);

        void spawnNewTracker(TrackerCommandVector &commands, cv::Rect2f &svmRect, ros::Time timestamp);

        bool calculateBEM(StampedTrackerState &state);

        void publishResults();

        int targetId;
    };

PPT_NS_END

#endif //PPT_PPT_H
