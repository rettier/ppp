//
// Created by reitph on 23.01.17.
//

#include "TrackerThread.h"
#include "PPT.h"

PPT_NS_START


    TrackerThread::TrackerThread(PPT *ppt) :
            WorkerThread("Tracker"), parent(ppt) {

    }

    TrackerThread::~TrackerThread() {

    }

    void TrackerThread::init() {
        trackerParams.enableTrackingLossDetection = true;
        trackerParams.psrThreshold = 13.5;
    }

    void TrackerThread::deduplicate(TrackerStateVector &states) {
        std::vector<int> idsToDelete;
        for (auto &state1 : states) {
            for (auto &state2 : states) {
                if (state2.id == state1.id)
                    continue;

                float intersection = (state1.rect & state2.rect).area();
                float percentage = intersection / std::min(state1.rect.area(), state2.rect.area());
                if (percentage > DEDUPLICATE_THRESH) {

                    // i feel like this can be written better
                    if (parent->targetId == state1.id || parent->targetId == state2.id) {
                        if (parent->targetId == state2.id) {
                            idsToDelete.push_back(state1.id);
                        } else {
                            idsToDelete.push_back(state2.id);
                        }
                    } else {
                        if (state1.rect.area() < state2.rect.area()) {
                            idsToDelete.push_back(state1.id);
                        } else {
                            idsToDelete.push_back(state2.id);
                        }
                    }
                }
            }
        }
        for (auto &id : idsToDelete) {
            auto it = std::find_if(states.begin(), states.end(),
                                   [&id](const StampedTrackerState &s) { return s.id == id; });

            if (it != states.end()) {
                log("deduplicate id %d", it->id);
                delete trackers[it->id];
                states.erase(it);
            }
        }
    }

    void TrackerThread::processCommands(TrackerStateVector &states, TrackerCommandVector &commands) {
        for (auto &cmd : commands) {
            auto stateIt = stateById(states, cmd.id);
            if (stateIt == states.end() && cmd.command != INIT_FROM_BACKLOG)
                continue;
            switch (cmd.command) {
                case INIT_FROM_BACKLOG:
                    newTrackerFromBacklog(cmd, states);
                    log("init target %d, %d active", lastId - 1, states.size());
                    break;

                case REINIT_FROM_BACKLOG:
                    log("reinit target %d (%d), %i active", cmd.id, stateIt->id, states.size());
                    reinitTracker(cmd, stateIt, states);
                    break;

                case KILL:
                    delete trackers[stateIt->id];
                    trackers.erase(stateIt->id);
                    states.erase(stateIt);
                    log("kill target %d, %d active", cmd.id, states.size());
                    break;

                default:
                    break;
            }
        }
    }

    TrackerStateVector::iterator TrackerThread::stateById(TrackerStateVector &states, int id) {
        if (id == 0)
            return states.end();

        for (auto state = states.begin(); state != states.end(); ++state) {
            if (state->id == id)
                return state;
        }
        return states.end();
    }

    void TrackerThread::work() {
        bool didProcessCommands = false;

        TrackerStateVector states = state.get();
        this->command.lock();
        if (command.var.size() > 0) {
            didProcessCommands = true;
            TrackerCommandVector commands = command.var;
            this->command.unlock();

            processCommands(states, commands);
        } else {
            this->command.unlock();
        }

        history.push_front(currentState);
        if (history.size() > HISTORY_SIZE)
            history.pop_back();

        track(states);
        deduplicate(states);

        this->state.set(states);

        // reset tracker commands only after processing, to minimize
        // race conditions
        if (didProcessCommands) {
            command.lock();
            command.var.clear();
            command.unlock();
        }
    }

    void TrackerThread::shutdown() {

    }

    void TrackerThread::track(TrackerStateVector &states) {
        for (auto &state : states) {
            if (state.state == TRACKING || state.state == LOST) {
                bool found = trackers[state.id]->update(currentState.cameraFrame, state.rect);
                if (!found && state.state == TRACKING) {
                    state.state = LOST;
                    state.lostAt = ros::Time::now();
                    log("lost target %i", state.id);
                } else if (found && state.state == LOST) {
                    state.state = TRACKING;
                    log("redetected target %i, active: %lu", state.id, states.size());
                }
            }
        }
    }

    void TrackerThread::reinitTracker(TrackerCommandMessage &cmd, TrackerStateVector::iterator &currentStateIt,
                                      TrackerStateVector &states) {
        if (currentStateIt == states.end())
            return;

        if (cmd.timestamp < history.back().timestamp)
            return;

        StampedTrackerState newState;
        newState.id = currentStateIt->id;
        newState.rect = constrainRect(cmd.rect);
        newState.lostAt = cmd.timestamp;
        newState.state = BACKLOG;

        auto *tracker = new KcfTracker(trackerParams);
        for (auto &state : history) {
            if (state.timestamp < cmd.timestamp)
                continue;

            if (newState.state == BACKLOG) {
                if (tracker->reinit(state.cameraFrame, newState.rect)) {
                    newState.state = TRACKING;
                }
            } else {
                if (tracker->update(state.cameraFrame, newState.rect)) {
                    newState.state = TRACKING;
                } else {
                    newState.state = LOST;
                }
            }
        }

        if (newState.state == TRACKING) {
            delete trackers[newState.id];
            trackers[newState.id] = tracker;
            states.erase(currentStateIt);
            states.push_back(newState);
        } else {
            delete tracker;
        }
    }

    void TrackerThread::padRect(cv::Rect2f &rect) {
        rect.x += rect.width * 0.5f * TRACKER_MARGIN_WIDTH;
        rect.width -= rect.width * TRACKER_MARGIN_WIDTH;
        rect.y += rect.height * 0.5f * TRACKER_MARGIN_HEIGHT;
        rect.height -= rect.height * TRACKER_MARGIN_HEIGHT;
    }

    void TrackerThread::newTrackerFromBacklog(TrackerCommandMessage &cmd, TrackerStateVector &states) {
        if (cmd.timestamp < history.back().timestamp)
            return;

        StampedTrackerState trackerState;
        trackerState.id = lastId++;
        trackerState.rect = constrainRect(cmd.rect);
        trackerState.lostAt = cmd.timestamp;
        trackerState.state = BACKLOG;

        auto *tracker = new KcfTracker(trackerParams);

        for (auto &state : history) {
            if (state.timestamp < cmd.timestamp)
                continue;

            if (trackerState.state == BACKLOG) {
                if (tracker->reinit(state.cameraFrame, trackerState.rect)) {
                    trackerState.state = TRACKING;
                }
            } else {
                if (tracker->update(state.cameraFrame, trackerState.rect)) {
                    trackerState.state = TRACKING;
                } else {
                    trackerState.state = LOST;
                }
            }
        }

        if (trackerState.state == TRACKING) {
            states.push_back(trackerState);
            trackers[trackerState.id] = tracker;
        } else {
            delete tracker;
        }
    }

PPT_NS_END