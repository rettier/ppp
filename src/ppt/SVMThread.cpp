#include "SVMThread.h"

#include <ros/package.h>

PPT_NS_START

    SVMThread::SVMThread() :
            WorkerThread("SVM") {

    }

    void SVMThread::init() {
        std::string packagePath = ros::package::getPath("ppt");
        std::ifstream fin(packagePath + "/" + DETECTOR_PATH, std::ios::binary);
        dlib::deserialize(detector, fin);
    }

    void SVMThread::work() {
        dlib::cv_image<uint8_t> image(currentState.cameraFrameGray);
        const std::vector<dlib::rectangle> rects = detector(image);

        if (rects.size() > 0) {
            state.set(StampedSVMDetection(rects, currentState.timestamp));
        }
    }

    void SVMThread::shutdown() {

    }


PPT_NS_END