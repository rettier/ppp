//
// Created by reitph on 22.01.17.
//

#include "ROSInterface.h"
#include "PPT.h"
#include <ppt/status.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>

PPT_NS_START

    ROSInterface::ROSInterface(PPT *ppt) :
            imageTransport(nodeHandle),
            ppt(ppt),
            enabled(true) {
        ppt->debug = this;
        ppt->publish = this;
    }

    void ROSInterface::init() {
        cameraSubscriber = imageTransport.subscribeCamera(IMAGE_TOPIC, 5, &ROSInterface::onCameraFrame, this);
        debugImagePublisher = imageTransport.advertise(DEBUG_TOPIC, 1);
        statusService = nodeHandle.advertiseService(STATUS_TOPIC, &ROSInterface::setStatus, this);
        poseArrayPublisher = nodeHandle.advertise<geometry_msgs::PoseArray>(POSE_TOPIC, 1);
        targetService = nodeHandle.advertiseService(TARGET_STATUS_TOPIC, &ROSInterface::setTarget, this);
        trackPublisher = nodeHandle.advertise<ppt::trackertarget>(TARGET_TRACK_TOPIC, 1);
        targetPosePublisher = nodeHandle.advertise<geometry_msgs::PoseStamped>(TARGET_POSE_TOPIC, 1);
    }

    bool ROSInterface::setTarget(ppt::target::Request &req, ppt::target::Response &res) {
        state.target_command = req.action;
        return true;
    }

    bool ROSInterface::setStatus(ppt::status::Request &req, status::Response &res) {
        switch (req.set_status) {
            case ppt::status::Request::STATUS_OFF:
                enabled = false;
                break;
            case ppt::status::Request::STATUS_ON:
                enabled = true;
                break;
            case ppt::status::Request::STATUS_UNCHANGED:
            default:
                break;
        }

        res.status = (enabled ? (uint8_t) ppt::status::Request::STATUS_ON : (uint8_t) ppt::status::Request::STATUS_OFF);
        return true;
    }

    void ROSInterface::spin() {
        ros::AsyncSpinner spinner(1);
        spinner.start();

        ros::Rate rate(30.0);
        while (nodeHandle.ok()) {
            tf::StampedTransform transform;
            try {
                //TODO: how can we always get the last transform?
                // at what rate is operator pose published?
                // somehow this always times me out
                //tfListener.waitForTransform(TF_CAMERA, TF_OPERATOR, ros::Time::now(), ros::Duration(10));
                // i dont know how to ros

                tfListener.lookupTransform(TF_CAMERA, TF_OPERATOR, ros::Time(0), transform);
            }
            catch (tf::TransformException &ex) {
                ROS_ERROR("%s", ex.what());
                ros::Duration(1.0).sleep();
                continue;
            }
            lastOperatorTransform = transform;
            rate.sleep();
        }
    };

    // this function projects a tf into the camera frame
    // it works if and only if the camera tf is placed wrong
    // luckily such a tf exists (camera_link)
    // if you switch to the (correct) camera_rgb_optical frame you have to remove
    // the mirroring (640-x, ...)
    ROSInterface::uvPair ROSInterface::transformOperatorToImage(const tf::StampedTransform &transform) {
        cv::Point2f uvFeet, uvHead;
        tf::Point ptFeet = transform.getOrigin();
        tf::Point ptHead = transform.getOrigin() + tf::Vector3(0.0, 0.0, OPERATOR_HEIGHT);
        uvFeet = cameraModel.project3dToPixel(tf2cv(ptFeet));
        uvHead = cameraModel.project3dToPixel(tf2cv(ptHead));
        uvHead.x = 640 - uvHead.x;
        uvFeet.x = 640 - uvFeet.x;
        uvHead.y = 480 - uvHead.y;
        uvFeet.y = 480 - uvFeet.y;
        return make_tuple(uvFeet, uvHead);
    }

    void ROSInterface::onCameraFrame(const sensor_msgs::ImageConstPtr &imageMsg,
                                     const sensor_msgs::CameraInfoConstPtr &infoMsg) {
        if (!enabled)
            return;

        cv::Mat image;
        cv_bridge::CvImagePtr inputBridge;
        try {
            inputBridge = cv_bridge::toCvCopy(imageMsg, sensor_msgs::image_encodings::BGR8);
            image = inputBridge->image;
        } catch (cv_bridge::Exception &ex) {
            PPT_LOG("Failed to convert image");
            return;
        }
        cameraModel.fromCameraInfo(infoMsg);

        cv::Mat out;
        cv::cvtColor(image, out, cv::COLOR_BGR2GRAY);
        state.cameraFrameGray = out;
        state.cameraFrame = image;
        state.timestamp = imageMsg->header.stamp;

        double timeSinceLastPose = (state.timestamp - lastOperatorTransform.stamp_).toSec();
        if (timeSinceLastPose > OPERATOR_POSE_TIMEOUT) {
            state.hasOperatorPose = false;
        } else {
            state.hasOperatorPose = true;
            tie(state.operatorBot, state.operatorTop) = transformOperatorToImage(lastOperatorTransform);
        }

        ppt->queueData(state);
        state.reset();
    }

    void ROSInterface::publishTargetTrack(const StampedROSState &state) const {
        ppt::trackertarget msg;
        msg.header.stamp = state.timestamp;
        msg.header.frame_id = TF_CAMERA;

        if (state.targetId != -1) {
            auto tl = state.targetRect.tl();
            auto size = state.targetRect.size();
            auto center = cv::Point2d((int) (tl.x + size.width * 0.5f), (int) (tl.y + size.height * 0.5f));
            cv::Point3d arrow = cameraModel.projectPixelTo3dRay(center);

            geometry_msgs::Pose pose;
            pose.position.x = 0.0;
            pose.position.y = 0.0;
            pose.position.z = 0.0;

            float pitch = (float) asin(-arrow.y);
            float yaw = (float) atan2(arrow.x, arrow.z);
            auto quat = tf::Quaternion(0.0, pitch, -yaw);

            pose.orientation.x = quat.x();
            pose.orientation.y = quat.y();
            pose.orientation.z = quat.z();
            pose.orientation.w = quat.w();
            msg.pose = pose;
        }
        msg.confidence = state.confidence;
        msg.state = (state.targetId == -1 ? (uint8_t) msg.STATE_LOST : (uint8_t) msg.STATE_TRACKING);


        // copy to plain StampedPose message for rviz
        geometry_msgs::PoseStamped poseMsg;
        poseMsg.header = msg.header;
        poseMsg.pose.orientation = msg.pose.orientation;
        poseMsg.pose.position = msg.pose.position;

        targetPosePublisher.publish(poseMsg);
        trackPublisher.publish(msg);
    }

    void ROSInterface::publishPoses(const TrackerStateVector &state) const {
        if (state.size() == 0)
            return;

        geometry_msgs::PoseArray arr;
        arr.header.stamp = ros::Time::now();

        for (auto a : state) {
            if (a.state != TRACKING)
                continue;

            auto tl = a.rect.tl();
            auto size = a.rect.size();
            auto center = cv::Point2d((int) (tl.x + size.width * 0.5f), (int) (tl.y + size.height * 0.5f));
            cv::Point3d arrow = cameraModel.projectPixelTo3dRay(center);

            geometry_msgs::Pose pose;
            pose.position.x = 0.0;
            pose.position.y = 0.0;
            pose.position.z = 0.0;

            float pitch = (float) asin(-arrow.y);
            float yaw = (float) atan2(arrow.x, arrow.z);
            auto quat = tf::Quaternion(0.0, pitch, -yaw);

            pose.orientation.x = quat.x();
            pose.orientation.y = quat.y();
            pose.orientation.z = quat.z();
            pose.orientation.w = quat.w();

            arr.header.stamp = a.timestamp;
            arr.poses.push_back(pose);
        }

        arr.header.frame_id = TF_CAMERA;
        poseArrayPublisher.publish(arr);
    }

    void ROSInterface::showImage(const cv::Mat &mat) const {
        if (mat.empty() || !enabled)
            return;
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", mat).toImageMsg();
        debugImagePublisher.publish(msg);
    }


PPT_NS_END